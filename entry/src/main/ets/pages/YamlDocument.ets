/*
* Copyright (c) 2022 Huawei Device Co., Ltd.

* Permission to use, copy, modify, and/or distribute this software for any purpose
* with or without fee is hereby granted, provided that the above copyright notice
* and this permission notice appear in all copies.

* THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
* REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
* INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
* OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
* TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
* THIS SOFTWARE.
*/

import  YAML, { Document }  from 'yaml'
import { Scalar, YAMLMap, YAMLSeq } from 'yaml/types'

@Entry
@Component
struct YamlDocument {
  @State documentList: Array<string> = ["", "", ""]
  @State yamlDocumentList: Array<string> = ["", "", ""]

  document() {
    const doc = new YAML.Document()
    doc.version = "true"
    doc.commentBefore = ' A commented document'
    doc.contents = ['some', 'values', { balloons: 99 }]
    this.yamlDocumentList[0] = String(doc)
  }

  parseDocument() {
    const src = '[{ a: A }, { b: B }]'
    const doc = YAML.parseDocument(src)
    const anchors: Document.Anchors = doc.anchors
    const contents :Scalar | YAMLMap | YAMLSeq | null= doc.contents
    const a: ESObject = contents == null ? null : contents["items"][0]
    const b: ESObject = contents == null ? null : contents["items"][1]
    this.yamlDocumentList[1] = String(doc)

    const alias = anchors.createAlias(a, 'AA')
    if (contents) contents["items"].push(alias)
    doc.toJSON()
    this.yamlDocumentList[2] = String(doc)
  }

  aboutToAppear() {
    this.document()
    this.parseDocument()
  }

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center}) {
      Text("YAML Document").fontSize(25).fontColor(Color.Gray).padding(20)
      Column({ space: 10 }) {
        Text("original data: \n' A commented document'\n['some', 'values', { balloons: 99 }]").fontSize(15)
        Text("YAMI.document data：\n" + this.documentList[0]).fontSize(15)
      }.alignItems(HorizontalAlign.Start).width("100%").padding({ left: 15, bottom: 25 })

      Column({ space: 10 }) {
        Text("original data: [{ a: A }, { b: B }]").fontSize(15)
        Text("YAMI.parseDocument data：\n" + this.documentList[1]).fontSize(15)
        Text("anchors.createAlias：\n" + this.documentList[2]).fontSize(15)
      }.alignItems(HorizontalAlign.Start).width("100%").padding({ left: 15, bottom: 25 })

      Button("YAML Document", { type: ButtonType.Normal, stateEffect: true }).borderRadius(8).width("50%")
        .onClick(() => {
          this.documentList[0] = this.yamlDocumentList[0]
          this.documentList[1] = this.yamlDocumentList[1]
          this.documentList[2] = this.yamlDocumentList[2]
        })
    }.width("100%")
  }
}