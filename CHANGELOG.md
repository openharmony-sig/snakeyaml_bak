## 2.0.0

- 适配DevEco Studio 版本： 4.1 Canary(4.1.3.317), OpenHarmony SDK:API11 (4.1.0.36)
- 新语法适配

## 1.0.2

- 适配DevEco Studio 3.1Beta1及以上版本。
- 适配OpenHarmony SDK API version 9及以上版本。

## 1.0.1

- api8升级到api9 stage模型

## 1.0.0

snakeyaml语言解析功能库：
支持：
Parse & Stringify,
YAML Documents等